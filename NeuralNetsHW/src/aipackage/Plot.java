package aipackage;

import java.awt.Dimension;

import javax.swing.JFrame;

import org.math.plot.Plot2DPanel;

/**
 * Using the jmathplot library
 * 
 * @author Samir Ahmed
 */
public class Plot {
	
	 private Plot2DPanel plotPanel ;
	 private JFrame frame ;

	 public Plot(String title){
		 this(title,"X","Y");
	 }
	 
	 public Plot(String title, String xAxisLabel, String yAxisLabel)
	 {
		 	this.plotPanel = new Plot2DPanel();
		 	this.plotPanel.addLegend("SOUTH");
		 	this.plotPanel.setAxisLabel(0, xAxisLabel);
		 	this.plotPanel.setAxisLabel(1, yAxisLabel);
		 	this.frame = new JFrame(title);
	        frame.setMinimumSize( new Dimension(600,600) );
	        frame.setVisible(false);
	 }
	 
	 public void draw()
	 {
		
		 frame.setContentPane( this.plotPanel );
		 frame.setVisible(true);
	 }
	 
	 public void addLinePlot( String name, double  [] yvalues)
	 {
		 this.plotPanel.addLinePlot(name, yvalues);
	 }
	
	 public static void plot(String name, double [] yvalues)
	 {
		 Plot p = new Plot(name);
		 p.addLinePlot(name, yvalues);
		 p.draw();
	 }
	 
//	 public static void main(String args[]){
//		 double[] test = new double[] {1.0,2.2,3.3,4.4,3.5,2.2,1.1,2.1,4.5,5.5,6.5,7.5,4.5};
//		 plot("Test",test);
//	 }
	
}
