package aipackage;

import java.io.FileNotFoundException;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * Static Execution File.
 * 
 * @author Samir Ahmed
 *
 */
public class TestNeuralNet {
	
	private final static double[] learningRates = new double[] {0.01,0.1,1,5,7.5,10,15};
	private final static int[] middleLayers = new int[] {2,5,10,20};
	private final static String lenseTrainingFile = "lenses.training" ;
	private final static String lenseTestingFile = "lenses.testing" ;
	private final static String creditTestingFile = "crx.data.testing";
	private final static String creditTrainingFile = "crx.data.training";
	
	
	/**
	 * Entry point for execution of the neural net training for lenses and credit data sets
	 * @param args
	 * @throws Exception
	 */
	public static void main(String [] args) throws Exception {
	
		verbose(false);
		
		//System.err.println("\nProcessing Credit Data\n\n");
		//creditData();
		
		System.err.println("\nProcessing Lenses Data\n\n");
		lenseData();
	}
	
	public static void lenseData() throws FileNotFoundException{
		
		System.err.println("==== LENSES TRAINING =====");
		// Create an 2D Matrix of layer size vs. Learning Rate
		// First Index will refer to the Node Size
		// Second Index will refer to the Node
		ArrayList< ArrayList< TestConfiguration > > tests = new ArrayList< ArrayList< TestConfiguration >> (middleLayers.length);
		
		// Populate the 2D array
		for ( int jj = 0; jj < middleLayers.length ; jj++)
		{
			ArrayList<TestConfiguration> test = new ArrayList<TestConfiguration> ( learningRates.length );
			
			// For every possible learning rate, create a new Test Configuration
			for ( int ii = 0; ii < learningRates.length ; ii++ )
			{
				// Load a new training data set, and create test instance for it.
				LenseLoader training = new LenseLoader(lenseTrainingFile);
				TestConfiguration config = new TestConfiguration( middleLayers[jj], learningRates[ii], training.m_inputvs, training.m_outputvs);
				test.add(config);
			}
			tests.add(test);
		}
		
		// Load the test data
		LenseLoader testing =  new LenseLoader(lenseTestingFile);
		
		// Now Train All of them
		int iterations  = 1000;
		for ( ArrayList<TestConfiguration> test : tests)
		{
			String title ="LENSES: Hidden Layer Size " + test.get(0).hiddenLayerSize();
			Plot pp = new Plot(title,"Training Iterations","Error");
			for ( TestConfiguration config : test )
			{
				// Train and capture the error
				config.train(iterations);
				double [] error = config.error();
				String label =  "r = "+config.learningRate();
				pp.addLinePlot(label, error);
				System.err.println("Hidden Layer : " +config.hiddenLayerSize() 
									+ "\tLearning Rate: " +config.learningRate() 
									+ "\tTraining Error: " + config.getTrainingErrorRate()
									+ "\tTesting Error: " + config.getTestingErrorRate(testing.m_inputvs, testing.m_outputvs));
			}
			pp.draw();
		}
		
	}

	public static void creditData()  throws FileNotFoundException {
		

		System.err.println("==== CREDIT TRAINING =====");
		// Create an 2D Matrix of layer size vs. Learning Rate
		// First Index will refer to the Node Size
		// Second Index will refer to the Node
		ArrayList< ArrayList< TestConfiguration > > tests = new ArrayList< ArrayList< TestConfiguration >> (middleLayers.length);
		
		// Populate the 2D array
		for ( int jj = 0; jj < middleLayers.length ; jj++)
		{
			ArrayList<TestConfiguration> test = new ArrayList<TestConfiguration> ( learningRates.length );
			
			// For every possible learning rate, create a new Test Configuration
			for ( int ii = 0; ii < learningRates.length ; ii++ )
			{
				// Load a new training data set, and create test instance for it.
				DataProcessor training = new DataProcessor(creditTrainingFile);
				TestConfiguration config = new TestConfiguration( middleLayers[jj], learningRates[ii], training.m_inputvs, training.m_outputvs);
				test.add(config);
			}
			tests.add(test);
		}
		
		// Load the test data
		DataProcessor testing =  new DataProcessor(creditTestingFile);
		
		// Now Train All of them
		int iterations  = 1000;
		for ( ArrayList<TestConfiguration> test : tests)
		{
			String title ="CREDIT: Hidden Layer Size " + test.get(0).hiddenLayerSize();
			Plot pp = new Plot(title,"Training Iterations","Error");
			for ( TestConfiguration config : test )
			{
				// Train and capture the error
				config.train(iterations);
				double [] error = config.error();
				String label =  "r = "+config.learningRate();
				pp.addLinePlot(label, error);
				System.err.println("Hidden Layer : " +config.hiddenLayerSize() 
									+ "\tLearning Rate: " +config.learningRate() 
									+ "\tTraining Error: " + config.getTrainingErrorRate()
									+ "\tTesting Error: " + config.getTestingErrorRate(testing.m_inputvs, testing.m_outputvs));
			}
			pp.draw();
		}
		
	}
	
	private static void verbose (boolean isVerbose) {

        if (!isVerbose){
                System.setOut( new PrintStream( new PipedOutputStream() ) );
        }
}

}
