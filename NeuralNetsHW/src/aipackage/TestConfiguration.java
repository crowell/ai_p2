package aipackage;

import java.io.FileNotFoundException;
import java.util.LinkedList;

/**
 * 
 * Test Configuration is wrapper class for testing and manipulating a Neural Network
 * 
 * @author Samir Ahmed
 *
 */
public class TestConfiguration
{
	// Testing variables
	private int[] layers;
	private double rr;
	private int count;
	
	/* private data members */
	private NeuralNet net;
	private DataProcessor testing;
	private DataProcessor data;
	private double [][] inputs; 
	private double [][] outputs;
	private LinkedList<Double> error ;  
	
	public int iterations() {
		return this.count;
	}
	
	public double learningRate() {
		return this.rr;
	}
	
	public int hiddenLayerSize() {
		return this.layers[1];
	}
		
	public TestConfiguration( int hiddenLayerSize, double learningRate, double [][] trainingIn, double [][] trainingOut) throws FileNotFoundException
	{
		// Error check the inputs
		if ( learningRate <=0.0 ) { throw new IllegalArgumentException("Learning Rate must be positive value"); }
		
		// 
		this.rr = learningRate;
		
		// Load the inputs and outputs into respective members
		this.inputs  = trainingIn;
        this.outputs = trainingOut;
        
        // Create a new ArrayList to dynamically store the errors as we make them
        this.error = new LinkedList<Double>();
        
		// Create a new neural net and set trained to false
        this.layers = new int[] {this.inputs[0].length, hiddenLayerSize, this.outputs[0].length};
		this.net = new NeuralNet(layers);
		this.net.connectAll();
		this.count = 0;
        
	}
	
	public void train(int iterations) 
	{

        for (int ii =0;  ii < iterations ; this.count++, ii++)
        {
        	// Train the neural net, and get the error
            this.net.train(this.inputs, this.outputs, this.rr);
            double error = this.net.error(this.inputs, this.outputs);

            // Save the error to file
            this.error.add(error);
            System.out.println("error is " + error);
        }  
		
	}
	
	public double getTrainingErrorRate()
	{
		return this.net.errorRate(this.inputs,this.outputs);
	}
	
	public double getTestingErrorRate(double[][] testInputs, double[][] testOutputs)
	{
		return this.net.errorRate(testInputs, testOutputs);
	}
	
	public double[] error() {
	
		// Convert the error to unboxed type and return
		Double [] values = this.error.toArray( new Double[this.error.size()] );
		double [] ee = new double [values.length];
		int ii =0;
		for (Double dd : values) { ee[ii++] = dd.doubleValue(); }
		return ee;
	}
	
}
